---
home: true
heroImage: /logo.png
tagline: 
actionText: Hop In →
actionLink: /guides/update-php.md
features:
- title: Change your panel's settings
  details: Settings that are not described in main documentation or updating specific case scenarios
- title: Customize your panel
  details: Different customization solutions
- title: Add new things
  details: Add features pterodactyl may need
footer: Made with ❤️
---
